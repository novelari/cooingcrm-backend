class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :update, :destroy]

  # GET /comments
  def index
    
    @comments = Comment.where(lead_id: params[:lead_id])
    
    @comments.each do |comment|
      comment.commenter_name = (User.find comment.user_id).name
      comment.commenter_id = (User.find comment.user_id).id
      comment.commenter_email = (User.find comment.user_id).email
    end
    
    render json: @comments, methods: [:commenter_name, :commenter_id, :commenter_email]
  end

  # GET /comments/1
  def show
    render json: @comment
  end

  # POST /comments
  def create
    @comment = Comment.new(comment_params)

    if @comment.save
      @comment.commenter_name = (User.find @comment.user_id).name
      @comment.commenter_id = (User.find @comment.user_id).id
      @comment.commenter_email = (User.find @comment.user_id).email

      render json: @comment, methods: [:commenter_name, :commenter_id, :commenter_email], status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      render json: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /comments/1
  def destroy
    @comment.destroy
    render json: @comment
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:message, :user_id, :lead_id)
    end
end
