class SchedulesController < ApplicationController
  before_action :set_schedule, only: [:show, :update, :destroy,:alert_date ]
  load_and_authorize_resource

  # GET /schedules
  def index
    if can? :read_all_schedules, @schedules
      @schedules = Schedule.all
    elsif can? :read_his_schedules, @schedules
      @schedules = Schedule.where({user_id: @current_user.id})
    end
    render json: @schedules
  end

  def get_schedules_per_lead
    @schedules = Schedule.where({user_id: @current_user.id, lead_id: params[:lead_id]})
    render json: @schedules
  end
  
  # GET /schedules/1
  def show
    render json: @schedule
  end

  # POST /schedules
  def create
    @schedule = Schedule.new(schedule_params)
    user = User.find(@schedule.user_id)
    lead = Lead.find(@schedule.lead_id)
    
    if @schedule.save
      render json: @schedule, status: :created
      ScheduleMailer.reminder(@schedule,user,lead).deliver_later(wait_until: @schedule.date)
      
    else
      render json: @schedule.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /schedules/1
  def update
    if @schedule.update(schedule_params)
      render json: @schedule
    else
      render json: @schedule.errors, status: :unprocessable_entity
    end
  end

  # DELETE /schedules/1
  def destroy
    @schedule.destroy 
    render json: @schedule

  end

  def alert_date
    @schedule = Schedule.where(schedule_params)
     render json: @schedule
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @schedule = Schedule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def schedule_params
      params.fetch(:schedule, {}).permit(:user_id, :lead_id, :date, :desc)
    end
end
