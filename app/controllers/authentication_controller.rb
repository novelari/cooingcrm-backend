require 'json_web_token'
class AuthenticationController < ApplicationController
  skip_before_action :authenticate_request
   def authenticate
      command = AuthenticateUser.call(params[:email].downcase, params[:password])

      if command.success?
        current_user = User.find_by(:email => params[:email].downcase)
        user = {}
        user[:details] = current_user
        user[:roles] = current_user.roles

        render json: { auth_token: command.result, user: user }
        else render json: { error: command.errors }, status: :unauthorized
      end
   end


end
