class LeadsController < ApplicationController
  before_action :set_lead, only: [:show, :update, :destroy,:AssignUser]
   load_and_authorize_resource


  # GET /leads
  def index
    # authorize! :read, @leads

    if can? :read_all_leads, @leads
       @leads = Lead.all

    else
      @leads = Lead.where(user_id: @current_user.id)

    end

    render json: @leads
  end

  # GET /leads/1
  def show
    user_email = @lead.user.email if @lead.user
    render json: {lead:@lead, user_email: user_email}
  end

  # POST /leads
  def create
    @lead = Lead.new(lead_params)

    if @lead.save
      render json: @lead, status: :created, location: @lead
    else
      render json: @lead.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /leads/1
  def update
    if @lead.update(lead_params)
      render json: @lead
    else
      render json: @lead.errors, status: :unprocessable_entity
    end
  end

  # DELETE /leads/1
  def destroy
    @lead.destroy
    render json: @lead
  end

  def AssignUser
    @user = User.joins(:leads).where(:leads => {:user_id => lead_params["user_id"]})
    # @lead = Lead.where(user_id:lead_params).find_by_id(lead_params)

    render json: @user
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lead
      @lead = Lead.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def lead_params

      params.fetch(:lead, {}).permit(
                                :name,
                                :user_id,
                                :status,
                                :name,
                                :date_of_first_contact,
                                :method_of_contact,
                                :telephone,
                                :email,
                                :comments,
                                :buyer,
                                :seller,
                                :best_time_to_call,
                                :desc,
                                :budget,
                                :date_of_trans,
                                :what_was_bought,
                                :unit_value,
                                :commission_earned,
                                :unit_value_currency,
                                :budget_currency
                                )



    end
end
