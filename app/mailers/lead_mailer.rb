class LeadMailer < ApplicationMailer
  default from: "elmahdy@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.lead_mailer.assign.subject
  #
  def assign(user, lead)
    @user = user
    @lead = lead

      mail to: user.email,
      subject: 'u assigned lead' ,
      body_type: lead.email
  end
end
