class ScheduleMailer < ApplicationMailer
  default from: "elmahdy@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.schedule_mailer.riminder.subject
  #
  def reminder(schedule,user,lead)
    @schedule = schedule
    @user = user
    @lead = lead
    # this is set in config/environments/development(production)
    @base_url = Rails.application.config.base_url

      mail to: user.email,
           subject: 'Reminder from Cooing-crm'
           
  end
end
