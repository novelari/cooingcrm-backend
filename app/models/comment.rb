class Comment < ApplicationRecord
    attr_accessor :commenter_name, :commenter_id, :commenter_email

    belongs_to :user
    belongs_to :lead

    def self.agents
      Comment.joins(:user)
        .select(:id, :name, :email)
    end
end
