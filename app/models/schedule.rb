class Schedule < ApplicationRecord
  validate :date_cannot_be_in_the_past

 def date_cannot_be_in_the_past
   if date.present? && date < Date.today
     errors.add(:date, "can't be in the past")
   end
 end


  belongs_to :user
  belongs_to :lead
end
