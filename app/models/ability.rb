 class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   access = false
    #   user.roles.each do |role|
    #     puts role
    #     access = true if role.role === 'sales_manager'
    #       puts 'access granted'
    #   end
    #
    #   if access
    #     can :manage, :all
    #   end
    # #
    puts 'initializer'
        user.roles.each do |role|
          type= role.role
          puts  role.role
        if type == "sales_manager"
            can :manage, :all
            can :read_all_leads, Lead
            can :read_all_schedules, Schedule
          elsif type == "sales_agent"
            can :read, Lead
            can :read_his_leads, Lead
            can :read_his_schedules, Schedule
          elsif type == "data_entry"
            can :read, Lead
            can :write, Lead
          end

    end
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
