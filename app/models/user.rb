class User < ApplicationRecord
    # validates :name, :email, :password,  presence: true
    # validates :name, :email, length: {minimum: 2, maximum: 100}
    # validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "not a valid email" }


    VALID_EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
    validates :email, presence: true,
    confirmation:true, format:{
    with: VALID_EMAIL_REGEX,
    message: "Enter a valid email address"
    }


    # validates :name, presence:true ,:length => {
    #  :minimum => 4,:message => 'please insert 4 characters as a minimum' }


    has_secure_password
    has_and_belongs_to_many :roles
    has_many :leads
    has_many :schedules
    has_many :comments


    def self.agents
      User.joins(:roles)
        .where(:roles => {:role => "sales_agent"})
        .select(:id, :name, :email)
    end
end
