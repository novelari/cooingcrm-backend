class Lead < ApplicationRecord
    after_initialize :set_defaults, unless: :persisted?

    validates :name, :status, :date_of_first_contact, :method_of_contact, :telephone, presence: true
    validates :name, length: {minimum: 2, maximum: 100}
    validates :telephone, length: {minimum: 4, maximum: 15}
    validates :status, inclusion: {
        in: ["Needs to be contacted",
             "Following up",
             "Waiting on new launch",
             "Meeting set up",
             "Sale Completed"]}
    validates :unit_value, :budget, numericality: {greater_than: 0, allow_nil:true}
    validate :date_of_first_contact_cannot_be_in_the_future
    validate :date_of_transaction_cannot_be_in_the_past

 
    VALID_EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
    validates :email, presence: true,
    confirmation:true, format:{
    with: VALID_EMAIL_REGEX,
    message: "Must insert a valid email address"
  }

    def set_defaults
        self.desc  ||= 'No description'
        self.buyer = false if self.buyer.nil?
        self.seller = false if self.seller.nil?
    end

    def date_of_first_contact_cannot_be_in_the_future
        if date_of_first_contact > Date.today
            errors.add("Date of first contact", "can't be in the future")
        end
        
    end

    def date_of_transaction_cannot_be_in_the_past
        if date_of_trans.present? && date_of_trans < Date.today
            errors.add("Date of transaction", "can't be in the past")
        end
        
    end
    

    belongs_to :user, optional: true
    has_many :schedules
    has_many :comments


end
