class Role < ApplicationRecord
    validates :role, presence: true
    has_and_belongs_to_many :users
end
