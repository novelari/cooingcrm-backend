require 'test_helper'

class LeadTest < ActiveSupport::TestCase
  #Todo
  # test "should not create lead without telephone, first_date_of_contact, " do
  #   assert true
  # end
  test "should not save name with null" do
    lead = Lead.new
    assert_not lead.save
  end

  test "lead attribute must not be empty" do
    lead = Lead.new
    assert lead.invalid?
    assert lead.errors[:name].any?
    assert lead.errors[:email].any?
    assert lead.errors[:telephone].any?
  end
end
