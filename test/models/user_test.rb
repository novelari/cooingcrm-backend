require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should not create user without name or email or password" do
    user1 = User.new
    assert !user1.save
    user2 = User.new(name:"foo",email:"foo@example.com",password:"123")
    assert user2.save

  end
  test "should only accept valid emails" do
    user = User.new(name:"foo",email:"foo.example.com",password:"123")
    assert !user.save
    user = User.new(name:"foo",email:"foo@example.com",password:"123")
    assert user.save

  end
end
