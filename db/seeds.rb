# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#seed for creating users
manager =User.create(name:"hossam", email:"hossam@gmail.com", password:"123")
agent =User.create(name:"ahmed", email:"okasha@gmail.com", password:"123")
entry = User.create(name:"mohamed", email:"mohamed@gmail.com", password:"123")
#seed for creating roles
manager.roles.create(role:"sales_manager")
agent.roles.create(role:"sales_agent")
entry.roles.create(role:"data_entry")

#seed for creating leads
 Lead.create(status:"Needs to be contacted", name:"karim",
 date_of_first_contact:"2017-09-20",
 method_of_contact:"Telephone",
 telephone:"0100199193",
 email:"karim@gmail.com",
 buyer:true, best_time_to_call:"1-month",
 desc:"call me as soon as",
 budget:"3000000",
 date_of_trans:"2017-12-12",
 what_was_bought:"apartment",unit_value:"3",
 commission_earned:"300",user_id:"2",
 LeadSource:"fb",LeadSubSource:"form",
 LstTimeFOF:"2017-03-19", FrstTimeReqTbContacted:"2013-03-02",
 LstReQTbContacted:"2017-08-01",budget_currency:"500000$",unit_value_currency:"2")

 Lead.create(status:"Following up", name:"okasha",
 date_of_first_contact:"2017-09-20",
 method_of_contact:"FaceBook",
 email:"okasha33@gmail.com",
 seller:true, best_time_to_call:"3-month",
 desc:"send email",
 budget:"130000",
  telephone:"0111199193",
 date_of_trans:"2017-11-10",
 what_was_bought:"villa",unit_value:"1",
 commission_earned:"5000",user_id:"2",
 LeadSource:"twitter",LeadSubSource:"wbesite post",
 LstTimeFOF:"2017-3-9", FrstTimeReqTbContacted:"2012-02-02",
 LstReQTbContacted:"2015-09-01",budget_currency:"300000$",unit_value_currency:"1")

 Lead.create(status:"Waiting on new launch",name:"menam",
 date_of_first_contact:"2011-11-10",
 method_of_contact:"CellPhone",
  telephone:"0123482222",
 email:"menam@gmail.com",
 buyer:true, best_time_to_call:"1-week",
 desc:"call me",
 budget:"13000",
 date_of_trans:"2014-2-2",
 what_was_bought:"villa",unit_value:"1",
 commission_earned:"30033",
 LeadSource:"email",
 LstTimeFOF:"2014-02-9", FrstTimeReqTbContacted:"2013-01-02",
 LstReQTbContacted:"2014-08-01",budget_currency:"4000000$",unit_value_currency:"1")

 Lead.create(status:"Meeting set up", name:"ahmed",
 date_of_first_contact:"2010-09-01",
 method_of_contact:"email",
 telephone:"0103199222",
 email:"elmahdy30@gmail.com",
 seller:true, best_time_to_call:"3-month",
 desc:"I sold 3 unit of villa",
 budget:"1300000",
 date_of_trans:"2014-12-12",
 commission_earned:"1300",
 LeadSource:"email",
 LstTimeFOF:"2013-03-19", FrstTimeReqTbContacted:"2011-03-02",
 LstReQTbContacted:"2017-08-01",budget_currency:"200000$",unit_value_currency:"1")

 Lead.create(status:"Sale Completed", name:"shimaa",
 date_of_first_contact:"2017-10-20",
 method_of_contact:"Telephone",
 telephone:"0123199193",
 email:"shima10@gmail.com",
 seller:true, best_time_to_call:"5-month",
 desc:"call me as soon as",
 budget:"2000000",
 date_of_trans:"2014-20-12",
 what_was_bought:"land",unit_value:"3",
 commission_earned:"300000",user_id:"2",
 LeadSource:"msg",LeadSubSource:"fb",
 LstTimeFOF:"2014-03-19", FrstTimeReqTbContacted:"2013-03-02",
 LstReQTbContacted:"2017-08-01",budget_currency:"300000$",unit_value_currency:"1")

# 10.times do
#     name = ('a'..'z').to_a.shuffle[0..5].join("")
#     email = "#{name}@example.com"
#     date = Date.new
#     telephone = rand(10*8)
#     status = rand(1..5)
#
#     Lead.create(name:name,email:email,date_of_first_contact:date,telephone:telephone,method_of_contact: "telephone",status:status,user_id:1)
# end

10.times do
    lead = rand(0..9)
    desc = ('a'..'z').to_a.shuffle[0..10].join("")
    schedule = agent.schedules.create(date: Date.new, desc:desc,lead_id:lead )

end
