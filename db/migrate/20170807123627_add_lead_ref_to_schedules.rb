class AddLeadRefToSchedules < ActiveRecord::Migration[5.1]
  def change
    add_reference :schedules, :lead, foreign_key: true
  end
end
