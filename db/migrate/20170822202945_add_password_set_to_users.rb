class AddPasswordSetToUsers < ActiveRecord::Migration[5.1]
  def change
      add_column :users, :password_set, :boolean
  end
end
