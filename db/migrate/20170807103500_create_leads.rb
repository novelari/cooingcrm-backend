class CreateLeads < ActiveRecord::Migration[5.1]
  def change
    create_table :leads do |t|
      t.integer :status
      t.string :name
      t.date :date_of_first_contact
      t.string :method_of_contact
      t.string :telephone
      t.string :email
      t.text :comments
      t.boolean :buyer
      t.boolean :seller
      t.string :best_time_to_call
      t.text :desc
      t.integer :budget
      t.date :date_of_trans
      t.text :what_was_bought
      t.integer :unit_value
      t.integer :commission_earned

      t.timestamps
    end
  end
end
