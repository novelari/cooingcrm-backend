class RemoveCommentsFromLeads < ActiveRecord::Migration[5.1]
  def change
    remove_column :leads, :comments
  end
end
