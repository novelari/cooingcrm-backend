class Leads < ActiveRecord::Migration[5.1]
  def change
    change_column :leads, :budget_currency, :string
    change_column :leads, :unit_value_currency, :string
  end
end
