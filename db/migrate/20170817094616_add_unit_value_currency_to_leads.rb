class AddUnitValueCurrencyToLeads < ActiveRecord::Migration[5.1]
  def change
    add_column :leads, :unit_value_currency, :integer
  end
end
