class AddCoulmnToLead < ActiveRecord::Migration[5.1]
  def change
    add_column :leads, :LeadSource, :string
    add_column :leads, :LeadSubSource, :string
    add_column :leads, :LstTimeFOF, :date
    add_column :leads, :FrstTimeReqTbContacted, :date
    add_column :leads, :LstReQTbContacted, :date  
  end
end
