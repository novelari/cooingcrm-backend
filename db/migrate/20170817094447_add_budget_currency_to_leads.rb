class AddBudgetCurrencyToLeads < ActiveRecord::Migration[5.1]
  def change
    add_column :leads, :budget_currency, :integer
  end
end
