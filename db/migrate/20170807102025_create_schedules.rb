class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
      t.datetime :date
      t.string :desc
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
