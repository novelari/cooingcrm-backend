class ChangeColumnFromIntegerToString < ActiveRecord::Migration[5.1]
  def change
    change_column :leads, :status, :string
  end
end
