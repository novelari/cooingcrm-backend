require_relative 'boot'
require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Back
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.active_record.observers = [:schedule_observer,:lead_observer]
    # config.active_record.observers = :schedule_observer


    config.autoload_paths << Rails.root.join('lib')
    config.active_job.queue_adapter = :delayed_job
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :path, :options]
      end
    end


    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    #setting email smtp
    config.action_mailer.default_url_options = {host:"localhost:3000"}
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
        :user_name => ENV["GMAIL_USERNAME"],
        :password => ENV["GMAIL_PASSWORD"],
        :domain => 'cooing.com',
        :address => 'smtp.gmail.com',
        :port => 587,
        :authentication => :plain,
        :enable_starttls_auto => true
        # :openssl_verify_mode  => 'none',

      }

  end
end
