Rails.application.routes.draw do
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/api' do

    resources :leads do
      resources :comments
      member do
        post :AssignUser
      end
    end
    resources :users do
      collection do
        get :getAgents
      end
    end

  end

  post 'authenticate', to: 'authentication#authenticate'

  # get  '/api/leads/:id/AssignUser/user_id', to: 'leads#AssignUser'
  get '/api/schedules', to: 'schedules#index'
  get '/api/:lead_id/schedules', to: 'schedules#get_schedules_per_lead'
  post '/api/schedules', to: 'schedules#create'
  delete '/api/schedules/:id', to: 'schedules#destroy'
  
  # comments

end
